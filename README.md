# AUTOVPNBOOK #
## Automatically Login And Connect To VPNBook ##
***

## Step 1: ##
* Download [autovpnbook repository](https://bitbucket.org/ruped24/autovpnbook/get/32d337d45bd7.zip) and unzip.

* Download All [VPNBOOK](https://www.vpnbook.com/freevpn)'s Server OpenVPN Certificate Bundles. **See Step 2**.

## Step 2: ##
* **get_vpnbook_bundle.sh** will download, setup, auto-launch avpnb.sh, and AutoVPNBook Menu.

### Usage: ###
```
#!bash

./get_vpnbook_bundle.sh
```

### Usage: ###

* **avpnb.sh** will launch AutoVPNBook Menu.

```
#!bash

sudo ./avpnb.sh
```

 



##### Disclaimer: ######

###### These scripts are written for educational purposes only!  

##### Legal Disclosure: #####

###### We are not affiliated with vpnbook.com in any way.  We are not advocating the use of their or any *[free](https://plus.google.com/117604887745850959716/posts/J8tkLw1Fgjv)* VPN service.

** [Screenshot](https://drive.google.com/open?id=0B79r4wTVj-CZNzBfOVpaRnRQMHc) ** :wink: