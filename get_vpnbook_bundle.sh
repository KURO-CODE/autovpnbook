#! /bin/bash
#
# Written by Rupe
# Download and setup vpnbook OpenVpn Certificate Bundle v.1.1
#

spinner() {
  local i sp n
  sp='/-\|'
  n=${#sp}
  printf ' '
  while sleep 0.1
  do
    printf "%s\b" "${sp:i++%n:1}"
  done
}

install_deps() {
  clear
  echo ""
  printf "\nInstalling dependencies, please wait..."
  spinner &
  sudo apt-get update -q=2 &> /dev/null
  sudo apt-get -yq=2 install $1 $2 &> /dev/null
  kill %1
  printf '\x1b[H\x1b[2J'
  echo -e "\n"
}

# Check for dependencies
declare -a deps=(openvpn python-pexpect)
[[ $(sudo which openvpn) ]] && \
[[ $(sudo dpkg --list | grep pexpect) ]]

# Install if missing dependencies
case $? in
  1) (install_deps ${deps[@]});;
  *) true;;
esac

cleanup() {
  rm VPN*.zip && rm $0 && \
  sudo ./avpnb.sh
}

trap cleanup Exit

dwnld_cert_bundle() {
  wget -r -l1 -H -t1 -nd -N -np -A.zip -erobots=off \
  http://www.vpnbook.com/#openvpn
  return $?
}

unpack_bundle() {
  for bundle in $(ls VPN*.zip)
  do
    unzip $bundle
  done
  return $?
}

auth_nocache() {
  for ovpn in $(ls *.ovpn)
  do
    sed -i '11i auth-nocache' $ovpn
  done
  return $?
}

get_username() {
  curl -s "http://www.vpnbook.com/#openvpn" \
  | grep -A 1 "Username: vpnbook" | tail -n 2 \
  | cut -f2 -d " " | cut -f1 -d '<' > username.txt
  return $?
}

get_password() {
  curl -s "http://www.vpnbook.com/#openvpn" \
  | grep -A 1 "Password:" | tail -n 2 | cut -d ':' -f2 \
  | cut -d '<' -f1 | tr -d ' ' > password.txt
  return $?
}

dwnld_cert_bundle && \
unpack_bundle && \
auth_nocache && \
get_username && \
get_password

exit $?